#!/usr/bin/env bash

FOLDER=$1 # Get the first input
OUTPUT_PATH=$2 # Get the second input
REQUIREMENTS="requirements.txt"
CUR_FOLDER="$PWD"

echo "- Starting: Lambda Function Build Process -"

if [ -z "$FOLDER" ] # Test if variable is set
then
  echo "\$FOLDER is empty. Won't change Folder. Looking for requirements file in this directory." # Variable is empty
else
  if [ -d "$FOLDER" ]; then # Test if directory exists
    echo "Directory exists. Changing directory to: $FOLDER" # Variable is set
    cd $FOLDER # Change directory
  else
    echo "Directory does not exist. Trying to find the requirements in this directory."
  fi
fi

if [ -f "$REQUIREMENTS" ]; then # Test if file exists
  echo "$FOLDER/$REQUIREMENTS exists. Trying to install dependencies." # File exists
  pip3 install -r $REQUIREMENTS -t . # Install requirements
else
  echo "Pip requirements file ($FOLDER/$REQUIREMENTS) does not exist. Won't install any dependencies." # File does not exist
fi

# echo "Compressing all contents of $FOLDER/" # File exists
# cd .. # Go back one folder
# zip -j -e $OUTPUT_PATH function/*

if [ "$CUR_FOLDER" != "$PWD" ]; then
  echo "Going back to start folder"
  cd $CUR_FOLDER # Go back to start directory
fi

echo "Script finished."

output "arn" {
  value = aws_lambda_function.this.arn
}

output "name" {
  value = aws_lambda_function.this.function_name
}

output "timeout" {
  value = aws_lambda_function.this.timeout
}

// output "role_arn" {
//   value = local.create_iam == 1 ? try(aws_iam_role.this.arn, "") : ""
// }

// output "role_name" {
//   value = local.create_iam == 1 ? try(aws_iam_role.this.name, "") : ""
// }

# AWS Lambda Auto Package

## Overview

This module is different to the "AWS Lambda Function module" (tf_module_aws_lambda).
This module needs a path to the Lambda Function (currently only Python) and installs all dependencies and packages the Lambda Function on its own. It also creates the needed IAM Roles.

## Resources created by this module

- Lambda Function
- IAM Role for Lambda Function, with:
  - Execution Role Policy (AssumeRole)
  - Anything that is defined in the custom_policy variable
  - Adds a Lambda Layer, if defined

## Example

```
module "lambda_network_tagging" {
  source      = "git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-lambda-autopackage?ref=master"

  name        = <FUNCTION-NAME>
  description = <FUNCTION-DESCRIPTION>
  handler     = "index.lambda_handler"
  policy      = templatefile("${path.module}/templates/iam/lambda_policy.json", {
    REGION   = data.aws_region.current.name,
    <OTHER>  = <REPLACE-IN-POLICY>
  })
  code_source = "${path.module}/templates/lambda/<LAMBDA-FOLDER>/function"
  code_output = "${path.module}/templates/lambda/<LAMBDA-FOLDER>/out"
  runtime     = "python3.6"
  timeout     = <TIMEOUT>
  memory_size = <MEMORY>
  env_vars = {
    AWS_ACCOUNT_ID  = data.aws_caller_identity.current.account_id,
    <OTHER>         = <OTHER-ENVIRONMENT-VARIABLE>
  }
  subnet_ids         = <SUBNET-ID>
  security_group_ids = <SECURITY-GROUP-ID>

  efs_access_point_arn = aws_efs_access_point.access_point_for_lambda.arn
  efs_mount_path       = "/mnt/efs"

  tags = merge(local.tags, {
    <TAG-NAME> = <TAG-VALUE>
  })
}
```

## Update History

### v1.0.3

- Added Support for EFS

### v1.0.1

- Lambda Module now accepts a list of AWS Subnet IDs and a List of AWS Security Group IDs
- Caution: Naming has changed from subnet_id /security_group_id to subnet_ids / security_group_ids

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.50 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.1.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_log_group.lambda](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_iam_role.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy.policies](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy_attachment.logs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.vpc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.xray](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_lambda_function.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | resource |
| [null_resource.buildstep](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [aws_iam_role.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_role) | data source |
| [aws_region.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [null_data_source.wait_for_script](https://registry.terraform.io/providers/hashicorp/null/latest/docs/data-sources/data_source) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_build_code"></a> [build\_code](#input\_build\_code) | Defines wether the Lambda code will be build via build.sh or not. If not, the prebuild code (.zip) will be directly taken from the code\_output path. | `bool` | `true` | no |
| <a name="input_code_output"></a> [code\_output](#input\_code\_output) | Path where the archive should be stored | `string` | n/a | yes |
| <a name="input_code_source"></a> [code\_source](#input\_code\_source) | Path to the Lambda Function | `string` | n/a | yes |
| <a name="input_custom_policy"></a> [custom\_policy](#input\_custom\_policy) | If a Policy should be created or not (default: yes) | `number` | `1` | no |
| <a name="input_cwl_retention_days"></a> [cwl\_retention\_days](#input\_cwl\_retention\_days) | The retention time in days for the CloudWatch Log Streams. | `number` | `180` | no |
| <a name="input_description"></a> [description](#input\_description) | Description for the Lambda Function | `string` | `""` | no |
| <a name="input_efs_access_point_arn"></a> [efs\_access\_point\_arn](#input\_efs\_access\_point\_arn) | ARN of the EFS Access Point | `string` | `""` | no |
| <a name="input_efs_mount_path"></a> [efs\_mount\_path](#input\_efs\_mount\_path) | Local mount path inside the lambda function. Must start with '/mnt/'. | `string` | `"/mnt/efs"` | no |
| <a name="input_env_vars"></a> [env\_vars](#input\_env\_vars) | Environment variables for the Lambda Function | `map(string)` | `null` | no |
| <a name="input_handler"></a> [handler](#input\_handler) | Handler function of the Lambda Function | `string` | n/a | yes |
| <a name="input_layers"></a> [layers](#input\_layers) | List of Lambda Layer Version ARNs (maximum of 5) to attach to your Lambda Function. Provide an empty list if no Lambda Layer is needed. | `list(string)` | `[]` | no |
| <a name="input_memory_size"></a> [memory\_size](#input\_memory\_size) | Memory size for the Lambda Function | `number` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | Name for the Lambda Function | `string` | n/a | yes |
| <a name="input_policy"></a> [policy](#input\_policy) | Path to a Policy for the Lambda Function (except the AWSLambdaBasicExecutionRole) | `string` | `""` | no |
| <a name="input_reserved_concurrent_executions"></a> [reserved\_concurrent\_executions](#input\_reserved\_concurrent\_executions) | The amount of reserved concurrent executions for this lambda function. | `number` | `-1` | no |
| <a name="input_runtime"></a> [runtime](#input\_runtime) | Runtime for the Lambda Function | `string` | n/a | yes |
| <a name="input_security_group_ids"></a> [security\_group\_ids](#input\_security\_group\_ids) | A List of one or more Security Groups for the Lambda function. Provide an empty list if no VPC Config is needed. | `list(string)` | `[]` | no |
| <a name="input_subnet_ids"></a> [subnet\_ids](#input\_subnet\_ids) | A List of one or more Subnets for the Lambda function. Provide an empty list if no VPC Config is needed. | `list(string)` | `[]` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags for the Lambda Function | `map(string)` | `{}` | no |
| <a name="input_timeout"></a> [timeout](#input\_timeout) | Timeout value for the Lambda Function | `number` | n/a | yes |
| <a name="input_xray_enabled"></a> [xray\_enabled](#input\_xray\_enabled) | Defines if AWS X-Ray is enabled for the Lambda Function. | `bool` | `true` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_arn"></a> [arn](#output\_arn) | n/a |
| <a name="output_name"></a> [name](#output\_name) | n/a |
| <a name="output_role_arn"></a> [role\_arn](#output\_role\_arn) | n/a |
| <a name="output_role_name"></a> [role\_name](#output\_role\_name) | n/a |
| <a name="output_timeout"></a> [timeout](#output\_timeout) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Pre-Commit Hooks

This Git Repository uses Pre-Commit Hooks.

- Download pre-commit from: https://pre-commit.com/ (`pip install pre-commit`)
- On first usage, run `pre-commit install` in this directory (this add the pre-commit hook to your git file)
- Now, everytime you run `git commit` the pre-commit tool runs and checks the files for issues defined in `.pre-commit-config.yaml`

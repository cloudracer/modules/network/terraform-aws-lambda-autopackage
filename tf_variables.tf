variable "name" {
  type        = string
  description = "Name for the Lambda Function"
}

variable "description" {
  type        = string
  description = "Description for the Lambda Function"
  default     = ""
}

variable "handler" {
  type        = string
  description = "Handler function of the Lambda Function"
}

variable "policy" {
  type        = string
  description = "Path to a Policy for the Lambda Function (except the AWSLambdaBasicExecutionRole)"
  default     = ""
}

variable "custom_policy" {
  type        = number
  description = "If a Policy should be created or not (default: yes)"
  default     = 1
}

variable "code_source" {
  type        = string
  description = "Path to the Lambda Function"
}

variable "code_output" {
  type        = string
  description = "Path where the archive should be stored"
}

variable "runtime" {
  type        = string
  description = "Runtime for the Lambda Function"
}

variable "timeout" {
  type        = number
  description = "Timeout value for the Lambda Function"
}

variable "memory_size" {
  type        = number
  description = "Memory size for the Lambda Function"
}

variable "reserved_concurrent_executions" {
  type        = number
  description = "The amount of reserved concurrent executions for this lambda function."
  default     = -1
}

variable "env_vars" {
  type        = map(string)
  description = "Environment variables for the Lambda Function"
  default     = null
}

variable "tags" {
  type        = map(string)
  description = "Tags for the Lambda Function"
  default     = {}
}

variable "layers" {
  type        = list(string)
  description = "List of Lambda Layer Version ARNs (maximum of 5) to attach to your Lambda Function. Provide an empty list if no Lambda Layer is needed."
  default     = []
}

variable "cwl_retention_days" {
  type        = number
  description = "The retention time in days for the CloudWatch Log Streams."
  default     = 180 # 6 months
}

variable "xray_enabled" {
  type        = bool
  description = "Defines if AWS X-Ray is enabled for the Lambda Function."
  default     = true
}

variable "build_code" {
  type        = bool
  description = "Defines wether the Lambda code will be build via build.sh or not. If not, the prebuild code (.zip) will be directly taken from the code_output path."
  default     = true
}

// VPC
variable "subnet_ids" {
  type        = list(string)
  description = "A List of one or more Subnets for the Lambda function. Provide an empty list if no VPC Config is needed."
  default     = []
}

variable "security_group_ids" {
  type        = list(string)
  description = "A List of one or more Security Groups for the Lambda function. Provide an empty list if no VPC Config is needed."
  default     = []
}

// EFS

variable "efs_access_point_arn" {
  type        = string
  description = "ARN of the EFS Access Point"
  default     = ""
}

variable "efs_mount_path" {
  type        = string
  description = "Local mount path inside the lambda function. Must start with '/mnt/'."
  default     = "/mnt/efs"
}

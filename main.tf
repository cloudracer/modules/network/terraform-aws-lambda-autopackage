// Identity & Access

// -- IAM Role
resource "aws_iam_role" "this" {
  name  = var.name # Roles Can only have (1 - 64) characters (which can be exceeded with logical naming...)
  assume_role_policy = templatefile("${path.module}/templates/iam/sts_assume_role.json",
    {
      service = "lambda.amazonaws.com"
  })
}

// -- Attach Default AWSLambdaBasicExecution Policy
resource "aws_iam_role_policy_attachment" "logs" {
  role       = aws_iam_role.this.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

// -- X-Ray
resource "aws_iam_role_policy_attachment" "xray" {
  count = var.xray_enabled ? 1 : 0

  role       = aws_iam_role.this.name
  policy_arn = "arn:aws:iam::aws:policy/AWSXrayWriteOnlyAccess"
}

// -- VPCAccess
resource "aws_iam_role_policy_attachment" "vpc" {
  role       = aws_iam_role.this.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

// - Set Lambda CloudWatch Log Group
resource "aws_cloudwatch_log_group" "lambda" {
  name              = format("/aws/lambda/%s", var.name)
  retention_in_days = var.cwl_retention_days
}

// -- Attach Custom Policy to the role
resource "aws_iam_role_policy" "policies" {
  count  = var.custom_policy
  name   = var.name
  role   = aws_iam_role.this.id
  policy = var.policy
}

// Lambda Function

// -- Install all (Python) Lambda Dependencies
resource "null_resource" "buildstep" {
  count = var.build_code ? 1 : 0
  triggers = {
    handler      = filebase64("${var.code_source}/${element(split(".", var.handler), 0)}.py")
    requirements = fileexists("${var.code_source}/requirements.txt") ? filebase64("${var.code_source}/requirements.txt") : "none"
    build        = filebase64("${path.module}/build.sh")
    output       = fileexists("${var.code_output}/${var.name}.zip") ? "none" : uuid()
  }

  provisioner "local-exec" {
    command = <<-EOF
      ${path.module}/build.sh ${var.code_source} ${var.code_output}/${var.name}.zip
      ${path.module}/package.sh ${var.code_source} ${var.code_output}/${var.name}.zip
    EOF
  }
}

data "null_data_source" "wait_for_script" {
  count = var.build_code ? 1 : 0
  inputs = {
    # This ensures that this data resource will not be evaluated until
    # after the null_resource has been created.
    script = null_resource.buildstep[0].id

    # This value gives us something to implicitly depend on
    # in the archive_file below.
    source_dir = "${var.code_output}/${var.name}.zip"
  }
}

// -- Lambda Function
resource "aws_lambda_function" "this" {
  function_name                  = var.name
  description                    = var.description
  handler                        = var.handler
  role                           = aws_iam_role.this.arn
  filename                       = "${var.code_output}/${var.name}.zip"
  source_code_hash               = var.build_code ? (fileexists(data.null_data_source.wait_for_script[0].outputs["source_dir"]) ? filebase64sha256(data.null_data_source.wait_for_script[0].outputs["source_dir"]) : "none") : (fileexists("${var.code_output}/${var.name}.zip") ? filebase64sha256("${var.code_output}/${var.name}.zip") : "none")
  runtime                        = var.runtime
  timeout                        = var.timeout
  memory_size                    = var.memory_size
  reserved_concurrent_executions = var.reserved_concurrent_executions
  dynamic "environment" {
    for_each = var.env_vars != null ? [1] : []
    content {
      variables = var.env_vars
    }
  }
  layers = var.layers
  tags   = var.tags

  tracing_config {
    mode = "Active"
  }

  // VPC Config
  dynamic "vpc_config" {
    for_each = var.subnet_ids == [] ? [] : [1]
    content {
      subnet_ids         = var.subnet_ids
      security_group_ids = var.security_group_ids
    }
  }

  // EFS Config
  dynamic "file_system_config" {
    for_each = var.efs_access_point_arn == "" ? [] : [1]
    content {
      arn              = var.efs_access_point_arn
      local_mount_path = var.efs_mount_path
    }
  }

  depends_on = [null_resource.buildstep]
}

#!/usr/bin/env bash

SOURCE_DIR=$1 # Get the first input
OUTPUT_PATH=$2 # Get the second input
ROOT_DIR=$PWD

echo "Starting: Script packaging process"

cd $SOURCE_DIR
mkdir -p $ROOT_DIR/$OUTPUT_PATH # Create out directory if it doesn't exist
rm -rf $ROOT_DIR/$OUTPUT_PATH # clear out directory
zip -r $ROOT_DIR/$OUTPUT_PATH .

echo "Packaging Script finished."
